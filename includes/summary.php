<?php
/**
 * @author: Renan Miranda
 * @comments: 
 * @purpose: summary page that'll display the survey entered data
 */
session_start();

//includes
require_once('session.php');
require_once('db_connection.php');
require_once('redirect.php');

//page control
$_SESSION["previous"] = "wizard_3.php";
$_SESSION["next"] = "thankyou.php";
setSession('current', 'includes/summary.php');


// printSession();

if($_SERVER['REQUEST_METHOD'] == 'POST'){

    if ($_POST["submit"] == 'previous') {
        redirect('previous');
    } else if ($_POST["submit"] == 'save') {
        $participantId = insertParticipant(getSession('full_name'), getSession('age'), getSession('student_options'));
        print_r($participantId);
        foreach (getSession('summarized') as $device => $responses) {                 
           
            insertResponse($participantId, $device, getSession('radio_purchase'), $responses['satisfaction'], $responses['recommendation']);      
        }
      
        // if success
        redirect('next');//thankyou page
    }
    //Array ( [Home Phone] => Array ( [satisfaction] => 1 [recommendation] => maybe ) 

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/materialize.css">
    <title>Survey - Summary</title>
</head>
<body>
    <div class="container">
            
            <div class="card-panel">
                <h3>Summary</h3>
                <div class="divider"></div>

            <div class="row">
                <form class="col s12" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" >
                    <table>
                    <thead>
                        <tr>
                            <th>Full Name</th>
                            <th>Age</th>
                            <th>Are you a student?</th>
                            <th>How did you complete your purchase?</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><?php echo getSession("full_name"); ?></td>
                            <td><?php echo getSession("age"); ?></td>
                            <td><?php synonym();?></td>
                            <td><?php echo getSession("radio_purchase"); ?></td>
                        </tr>
                    </tbody>
                    </table>

                <h4>Survey</h4>
                <div class="divider"></div>
                    <table>
                    <thead>
                        <tr>
                            <th>Which of the following did you purchase?</th>
                            <th>How happy are you with this device?</th>
                            <th>Would you recommend the purchase of this device to a friend?</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                    //[summarized] => Array ( [Home Phone] => Array ( [satisfaction] => Not Satisfied [recommendation] => yes )  ) )
                        foreach (getSession('summarized') as $device => $responses) {//[summarized] => Array ()                     
                            echo '<tr>';
                            echo '<td>'.$device.'</td>';
                                foreach ($responses as $response) {//it's going to display what was selected | [Home Phone] => Array ( [satisfaction] => Not Satisfied [recommendation] => yes )
                                    echo '<td>'.$response.'</td>';                            
                                }
                            echo '</tr>';
                        }
                    ?>
                    </tbody>
                    </table>
                    
                
            </div>
                <div class="row">
                    <button class="btn-large" type="submit" name="submit" value="previous">Previous</button>
                    <button class="btn-large" type="submit" name="submit" value="save">Save</button>
                </div>
            </form>
            </div> <!-- end card-pannel-->
    </div><!-- end container-->
</body>
</html>
<?php
    function synonym(){
        if(getSession("student_options") == "f"){
             echo "fulltime"; 
        }
        elseif(getSession("student_options") == "p"){
            echo "parttime"; 
       }
       else{
           echo "Not student";
       }
    }
?>