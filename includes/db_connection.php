<?php 
/**
 * @author: Renan Miranda
 * @comments: operations executed on mysql database using PDO library
 * @purpose: database connection
 */
define("DBHOST", "localhost");
define("DBCATALOG", "lamp1_survey");
define("DBUSER", "lamp1_survey");
define("DBPASS", "!Survey9!");

    function connectDB() {
        $dsn = "mysql:host=".DBHOST.";dbname=".DBCATALOG.";charset=utf8";
        try {
            $db_conn = new PDO($dsn, DBUSER, DBPASS);
            return $db_conn;
        } catch (PDOException $th) {
            echo "<p>Error opening database ".$th->getMessage()."</p>";
            exit(1);
        }
    }

    function insertParticipant($full_name, $age, $is_student){
        $db_conn = connectDB();
        $stmt = $db_conn->prepare('INSERT INTO participants (part_fullname,part_age,part_student) values(:full_name, :age, :is_student)');
        if (!$stmt){
            echo "Error ".$db_conn->errorCode()."\nMessage ".implode($db_conn->errorInfo())."\n";
            exit(1);
        }
        $data = array(":full_name" => $full_name, ":age" => $age, ":is_student" => $is_student);
        $status = $stmt->execute($data);
        if(!$status){
               echo "Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."\n";
                exit(1);
        }

         $id = $db_conn->lastInsertId();

         return $id;
    }
  

    function insertResponse($resp_part_id, $resp_product, $resp_how_purchased, $resp_satisfied, $resp_recommend)
    {
        $db_conn = connectDB();
        $stmt = $db_conn->prepare('INSERT INTO responses (resp_part_id, resp_product, resp_how_purchased, resp_satisfied,resp_recommend) values(:resp_part_id, :resp_product, :resp_how_purchased, :resp_satisfied, :resp_recommend)');
        if (!$stmt){
            echo "Error ".$db_conn->errorCode()."\nMessage ".implode($db_conn->errorInfo())."\n";
            exit(1);
        }
        $data = array(":resp_part_id" =>$resp_part_id, ":resp_product"=>$resp_product, ":resp_how_purchased"=>$resp_how_purchased, ":resp_satisfied" => $resp_satisfied, ":resp_recommend"=>$resp_recommend);
        $status = $stmt->execute($data);
        if(!$status){
               echo "Error ".$stmt->errorCode()."\nMessage ".implode($stmt->errorInfo())."\n";
                exit(1);
        }
    }
?>