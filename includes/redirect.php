<?php
    require_once('session.php');

    function redirect($direction)
    {
        // $to = "includes/";

        if (getSession("previous") == 'index.php') {
            $to = "";
        }
        
        if ($direction == "next") {
            $to = 'includes/'.getSession("next");
        } else if ($direction == "previous") {
            $to = 'includes/'.getSession("previous");
        }        

        header('Location: http://localhost/project1/'.$to);
        exit();
    }

    function returnLastStage()
    {

       if (isset($_SESSION['current'])) {
           $stage = $_SESSION['current'];
           unset($_SESSION['current']);
            header('Location: http://localhost/project1/'.$stage);
            exit();
       }
        
    }

?>