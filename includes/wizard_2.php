<?php

/**
 * @author: Mohamad Arafat
 * @comments: 
 * @purpose: wizard page 2, get the user purchases and how was the purchase completed
 */
session_start();

require_once("session.php");
require_once("redirect.php");


//page control
$_SESSION["previous"] = "wizard_1.php";
$_SESSION["next"] = "wizard_3.php";
setSession('current', 'includes/wizard_2.php');


//printSession();

?>
<!DOCTYPE html>
<html lang="en">

<pre>
<?php

$purComplete = $purchase = "";
$purCompleteError = $purchaseError = "";



if ($_SERVER['REQUEST_METHOD'] == 'POST') {
  if ($_POST["submit"] == 'previous') {
    redirect('previous');
  }

  if (!isset($_POST['purchaseMethod']) && !isset($_POST["purchaseCart"])) {
    $purCompleteError = "You must select how you completed your purchase";
    $purchaseError = "You must select at least 1 option";
  } else if (!isset($_POST['purchaseMethod']) && isset($_POST['purchaseCart'])) {
    $purCompleteError = "You must select how you completed your purchase";
  } else if (isset($_POST['purchaseMethod']) && !isset($_POST['purchaseCart'])) {
    $purchaseError = "You must select at least 1 option";
  } else {
    $purComplete = $_POST['purchaseMethod'];
    $purchase = $_POST['purchaseCart'];

   
    $_SESSION["radio_purchase"] = $_POST['purchaseMethod'];
    $_SESSION["checkbox_purchase"] = $_POST['purchaseCart'];

    redirect('next');

  }

}

?>
</pre>

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  <link rel="stylesheet" href="../css/style.css">
  <link rel="stylesheet" href="../css/materialize.css">
  <title>Survay - Purchase Page</title>
</head>

<body>

  <div class="container">

    <div class="card-panel">
      <h3>Purchase Page</h3>
      <div class="divider"></div>

      <div class="row">

        <form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
          <h5>How did you complete your purchase?</h5>
          <span class="helper-text" data-error="Invalid Selection" style=color:red;><?php echo $purCompleteError ?></span>
          <p>
            <label>
              <input <?php if (getSession("radio_purchase") == 'online') { ?>checked="true" <?php }; ?> name="purchaseMethod" type="radio" value="online" />
              <span>Online</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if (getSession("radio_purchase") == 'By phone') { ?>checked="true" <?php }; ?> name="purchaseMethod" type="radio" value="By phone" />
              <span>By Phone</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if (getSession("radio_purchase") == 'Mobile App') { ?>checked="true" <?php }; ?> name="purchaseMethod" type="radio" value="Mobile App" />
              <span>Mobile App</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if (getSession("radio_purchase") == 'Store') { ?>checked="true" <?php }; ?>name="purchaseMethod" type="radio" value="Store" />
              <span>In Store</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if (getSession("radio_purchase") == 'By mail') { ?>checked="true" <?php }; ?>name="purchaseMethod" type="radio" value="By mail" />
              <span>By Mail</span>
            </label>
          </p>

          <h5>Which of the following did you purchase?</h5>
          <span class="helper-text" data-error="Invalid Selection" style=color:red;><?php echo $purchaseError ?></span>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Home Phone", getSession('checkbox_purchase'))) echo "checked='checked'"; ?> type="checkbox" name="purchaseCart[]" value="Home Phone" />
              <span>Home Phone</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Mobile Phone", getSession('checkbox_purchase'))) echo "checked='checked'"; ?> type="checkbox" name="purchaseCart[]" value="Mobile Phone" />
              <span>Mobile Phone</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Smart TV", getSession('checkbox_purchase'))) echo "checked='checked'"; ?> type="checkbox" name="purchaseCart[]" value="Smart TV" />
              <span>Smart TV</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Laptop", getSession('checkbox_purchase'))) echo "checked='checked'"; ?>  type="checkbox" name="purchaseCart[]" value="Laptop" />
              <span>Laptop</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Desktop Computer", getSession('checkbox_purchase'))) echo "checked='checked'"; ?>  type="checkbox" name="purchaseCart[]" value="Desktop Computer" />
              <span>Desktop Computer</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Tablet", getSession('checkbox_purchase'))) echo "checked='checked'"; ?>  type="checkbox" name="purchaseCart[]" value="Tablet" />
              <span>Tablet</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("Home Theate", getSession('checkbox_purchase'))) echo "checked='checked'"; ?>  type="checkbox" name="purchaseCart[]" value="Home Theater" />
              <span>Home Theater</span>
            </label>
          </p>
          <p>
            <label>
              <input <?php if(getSession('checkbox_purchase') != '' && in_array("MP3 player", getSession('checkbox_purchase'))) echo "checked='checked'"; ?>  type="checkbox" name="purchaseCart[]" value="MP3 player" />
              <span>MP3 player</span>
            </label>
          </p>
          <div class="row">
            <button class="btn-large" type="submit" name="submit" value="previous">Previous</button>
            <button class="btn-large" type="submit" name="submit" value="next">Next</button>
          </div>
        </form>

        <?php
        if (isset($_POST['submit'])) {
          if (!empty($_POST['purchaseCart'])) {
            foreach ($_POST['purchaseCart'] as $selected) {
              echo $selected. "</br>";
            }
          }
        }
        ?>

      </div>
    </div><!-- card panel-->
  </div><!-- container-->

  <script src="../js/materialize.js"></script>
  <script src="../js/script.js"></script>
</body>

</html>