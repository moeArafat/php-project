<?php 
/**
 * @author: Renan Miranda
 * @comments: 
 * @purpose: wizard page 1, gathering user's information
 */
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/materialize.css">
    <title>Survay - Personal Info</title>
</head>
<body>

    <div class="container">
        <h3>Personal Info</h3>
        <div class="divider"></div>

        <div class="row">
            <form class="col s12" action="" method="post">
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" name="full_name" id="full_name">
                        <label for="full_name">Full Name</label>
                    </div>
                    <div class="input-field col s6">
                        <input type="text" name="age" id="age">
                        <label for="age">Age</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <select name="student_options" id="student_options">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="full_time">Yes, Full Time</option>
                            <option value="part_time">Yes, Part-time</option>
                            <option value="no">No</option>
                        </select>
                        <label>Are you a student?</label>

                    </div>
                </div>
            </form>

        </div>
    </div>
    

    <script src="js/materialize.js"></script>
    <script src="js/script.js"></script>
</body>
</html>